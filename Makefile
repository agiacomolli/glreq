all: glreq chown

glreq:
	sudo docker run --rm -e CGO_ENABLED=0 -v $(PWD):/go \
		golang:1.8 go build -v -o bin/glreq glreq

chown:
	sudo chown $(USER).$(USER) -R bin

clean:
	rm -rf bin

