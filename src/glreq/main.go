// This application will get HTTP response times over 5 minutes
// to `https://gitlab.com`.

package main

import (
    "io/ioutil"
    "log"
    "net/http"
    "time"
)

func main() {
    // Do requests after 5 minutes plus request time.
    for {
        log.Printf("new request")

        // Get request.
        resp, err := http.Get("https://gitlab.com")
        if err != nil {
            log.Fatal(err)
        }

        defer resp.Body.Close()

        log.Printf("response status: %s", resp.Status)

        // Print request body if received ok status.
        if resp.StatusCode == 200 {
            body, err := ioutil.ReadAll(resp.Body)
            if err != nil {
                log.Fatal(err)
            }

            log.Printf("%s", body)
        }

        // Wait sleep time using channels.
        timer := time.NewTimer(5 * time.Minute)
        <-timer.C
    }
}

